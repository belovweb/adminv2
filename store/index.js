import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      isSidebarOpened: false
    },
    mutations: {
      toggleSidebar (state) {
        state.isSidebarOpened = !state.isSidebarOpened;
      },
      openSidebar (state) {
        state.isSidebarOpened = true;
      },
      closeSidebar (state) {
        state.isSidebarOpened = false;
      }
    }
  })
}

export default createStore